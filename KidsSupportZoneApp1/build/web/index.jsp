<!--Author: Asyraf

-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>Thoughts</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content=""/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->

	<!-- css files -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Encode+Sans+Condensed:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext,vietnamese"
	    rel="stylesheet">
	<!-- //web-fonts -->
</head>

<body>
	<!-- title -->
	<h1>
		This is where you can share your thoughts.
	</h1>
	<!-- //title -->

	<!-- content -->
	<div class="container">
		<div id="clouds">
			<div class="cloud x1"></div>
			<!-- Time for multiple clouds to dance around -->
			<div class="cloud x2"></div>
			<div class="cloud x3"></div>
			<div class="cloud x4"></div>
			<div class="cloud x5"></div>
		</div>
		<!-- content form -->
		<div class="sub-main-w3">
			<form action="Validation" method="post">
				<div class="form-style-agile">
					<label>
						<i class="fas fa-user"></i>
						Email

					</label>
					<input placeholder="Email" name="email" type="text" required="">
				</div>
				<div class="form-style-agile">
					<label>
						<i class="fas fa-unlock-alt"></i>
						Password

					</label>
					<input placeholder="Password" name="password" type="password" required="">
				</div>
				<input type="submit" value="Log In">
			</form>
		</div>
		<!-- //content form -->
	</div>
	<!-- //content -->

	<!-- copyright -->
	<div class="footer">
            <h2>&copy; Asyraf Jamil<br>
			<a href="userRegister.jsp"> Register now if you don't have an account. </a>
		</h2>
	</div>
	<!-- //copyright -->


</body>

</html>