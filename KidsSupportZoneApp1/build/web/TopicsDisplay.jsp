<%-- 
    Document   : TopicsDisplay
    Created on : Dec 7, 2018, 4:37:52 PM
    Author     : ACER
--%>
<%@page import="com.model.User"%>
<%@ page import="com.model.Topic"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="id" value="${param.id}"></c:set>
<sql:setDataSource var="ksz" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/kidsupportzone" user="root" password=""/>
<jsp:useBean id="curUser" class="com.model.User" scope="request"></jsp:useBean>
<%
    curUser = (User) request.getSession().getAttribute("CurUser");
%>
<jsp:useBean id="CurrentTopic" class="com.model.Topic" scope="request"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<sql:query var="cur" dataSource="${ksz}">
    SELECT * FROM topics where id = ?
    <sql:param value="${id}"/>
</sql:query>
    <c:forEach var="data" items="${cur.getRowsByIndex()}">
        <jsp:setProperty name="CurrentTopic" property="id" value="${data[0]}"/>
        <jsp:setProperty name="CurrentTopic" property="pId" value="${data[1]}"/>
        <jsp:setProperty name="CurrentTopic" property="topic" value="${data[2]}"/>
        <jsp:setProperty name="CurrentTopic" property="desc" value="${data[3]}"/>
        <jsp:setProperty name="CurrentTopic" property="comments" value="${data[4]}"/>
    </c:forEach>

    <%
        String[] com = (CurrentTopic.getComments()).split("##");
    %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= CurrentTopic.getTopic() %></title>
    </head>
    <body>
        <h1>Hello <%=curUser.getName()%> id = <%=curUser.getId()%> <%=curUser.getEmail()%> </h1>
        <table border="1" width="100%">
            <thead>
                <tr>
                    <th>Topic</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <%=CurrentTopic.getTopic() %>
                        <br><br>
                        <sql:query var="usr" dataSource="${ksz}">
                            SELECT nick_name FROM user where id = ?
                            <sql:param value="${CurrentTopic.getpId()}"/>
                        </sql:query>
                            <c:forEach var="user" items="${usr.getRowsByIndex()}">
                                Posted By : ${user[0]}
                            </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td>Description : </td>
                </tr>
                <tr>
                    <td>
                        <br> <%= CurrentTopic.getDesc() %>
                    </td>
                </tr>
                <tr>
                    <td>Comments : </td>
                </tr>
                <tr>
                    <td>
                        <%
                            for(int x = 0;x<com.length;x++){
                                String[] thiscom = com[x].split("naiwa");
                        %>
                            <sql:query var="comuser" dataSource="${ksz}">
                                SELECT nick_name FROM user where id = <%=thiscom[0]%>
                            </sql:query>
                                <c:forEach var="thiscomm" items="${comuser.getRowsByIndex()}">
                                    ${thiscomm[0]} said : <%=thiscom[1]%>
                                    <%
                                        com[x] = com[x].replace("'", "`");
                                    %>
                                    <input type="button" value="Delete" onclick="deleting(<%="'"+com[x]+"'"%>,<%=curUser.getId()%>,<%=x%>,${CurrentTopic.getId()});">
                                    <input type="button" value="Edit" onclick="edit(<%="'"+com[x]+"'"%>,<%=x%>,<%=curUser.getId()%>);">
                                    <br><br>
                                </c:forEach>
                        <%}%>
                    </td>
                </tr>
            </tbody>
        </table>
                    <form name="commenting" method="POST" action="exComment">
                        <input type="hidden" name="curInd" id ="curInd" value="-1">
                        <input type="hidden" name="topicId" value="${CurrentTopic.getId()}"/>
                        <input type="hidden" name="userId" value="<%=curUser.getId()%>"/>
                        <textarea id="newcomm" name="newcomm"rows="10" cols="100" placeholder="Enter comments here" ></textarea><br>
                        <button type="submit">Submit</button>
                    </form>
                        <br><br><a href="ChooseTopic.jsp">Return to lists of topics</a>
    </body>
    <script>
       function deleting(com,id,x,y){
           var cur = com.split("naiwa");
           if(parseInt(cur[0])===id){
                var conf = confirm("Are you sure you want to delete this comment?");
                if(conf)window.location.replace("exDelete?ind="+x+"&id="+y);
            }
            else alert("You don't have permission to do this.");
       } 
       function edit(com,ind,id){
           var cur = com.split("naiwa");
           if(parseInt(cur[0])===id){
                var e = document.getElementById("newcomm");
                e.value = cur[1];
                var f = document.getElementById("curInd");
                console.log(f);
                f.value = ind;
            }
            else alert("You don't have permission to do this.");
       }
    </script>
</html>
