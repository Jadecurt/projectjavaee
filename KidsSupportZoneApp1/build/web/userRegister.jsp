<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : userRegister
    Created on : Nov 20, 2018, 10:26:45 PM
    Author     : Habib
--%>
<html>
<head>
<style>

.footer {
    background-color: black;
    padding: 10px;
	position:fixed;
	left:0px;
	bottom:0px;
	height:30px;
	width:100%;
}

.button {
    background-color: #9400F9;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
	width: 15%;
	height: 15%
}

.button:hover {background-color: #AE39FF}


body  {
    background-image: url("images/firewatch-video-game-desktop-wallpaper-59162-60946-hd-wallpapers.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;
	background-size: 100%;
	margin: 0;
}

.navbar {
    overflow: hidden;
    background-color: black;
    font-family: Arial;
	position: fixed;
	top: 0;
	width: 100%;
}

.navbar a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.dropdown {
    float: left;
    overflow: hidden;
	
}

.dropdown .dropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
}

.navbar a:hover, .dropdown:hover .dropbtn {
    background-color: #9400F9;
}

.dropdown-content {
    display: none;
    background-color: #f9f9f9;
	position: fixed;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {
    background-color: #ddd;
}

.dropdown:hover .dropdown-content {
    display: block;
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
    border-right:1px solid #bbb;
}

li:last-child {
    border-right: none;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}


.active {
    background-color: #4CAF50;
}

input[type=text], [type="file"], [type="date"], [type="password"], [type="email"],  select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #9400F9;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #AE39FF;
}



</style>


<title>KIDS SUPPORT ZONE.</title>

<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register User</title>
    </head>
    <body>
    <center>
        <a href="index.jsp">Home</a><br>
    <jsp:useBean id="userObj" class="com.model.User" scope="request"></jsp:useBean>
    <jsp:useBean id="errList" class="java.util.LinkedList" scope="request"></jsp:useBean>
        <strong>Please enter the following detail :</strong><br/>
        
        <div style="margin-top: 100px;"><h1><font color="black">Enter Your Details.</font><h1></div>
        <div style="margin-top: 50px; border-radius: 5px; background-color: #EAEAEA; padding: 20px; width: 40%;">
            <form name="myData" method="post"  action="exUserRegister.jsp">
            
	<label for="email">Email</label>
	<input type="text" id="email" name="email" placeholder="Enter your email..." value="${userObj.email}" required>
        
	<label for="name">Name</label>
	<input type="text" id="name" name="name" placeholder="Enter your name..." value="${userObj.name}" required>
	
        <label for="name">Nick Name</label>
	<input type="text" id="name" name="nick_name" placeholder="Create a nick name for your profile..." value="${userObj.nick_name}" required>
        
	<label for="age">Age</label>
	<input type="text" id="age" name="age" placeholder="Enter your age..." value="${userObj.age}" required>
	
	<label for="gender">Gender</label>
        <select id="gender" name="gender">
            <option value="male">
                Male
            </option>
            <option value="female">
                Female
            </option>
        </select>
	
        <label for="password">Password</label>
	<input type="password" id="password" name="password" placeholder="Enter your password..." value="${userObj.password}" required>
        <input type="hidden" name="role"/>
            
       
        <input name="submit" type="submit" value="REGISTER" onclick="confirmRole(${role});">
        </form>
        </div>
                    
        <%
            if(!errList.isEmpty()){
        %>
        <p>
            <font color='red'>Please correct the following errors:
                <ul>
                    <%
                        Iterator items = errList.iterator();
                        while(items.hasNext()){
                            String message =  (String)items.next();
                    %>
                    <li><%= message %></li>
                    <%
                        }
                    %>
                </ul>
            </font>
        </p>
        <%
            }
        %>
        </center>
        <script type="text/javascript">
        function confirmRole(role){
            confirm("You are registering as a "+role+". Continue?");
        }
        function alertName(){
            alert("You have been registered!");
            window.onload = alertName;
} 
        </script> 
    </body>
</html>

