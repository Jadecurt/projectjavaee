<%-- 
    Document   : ChooseTopic
    Created on : Dec 10, 2018, 2:47:46 AM
    Author     : ACER
--%>
<%@ page import="com.model.Topic"%>
<%@page import="com.model.User" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="ksz" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/kidsupportzone" user="root" password=""/>
<%--<%@include file="session.jsp" %>--%>
<jsp:useBean id="curUser" class="com.model.User" scope="request"></jsp:useBean>
<%
    curUser = (User) request.getSession().getAttribute("CurUser");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Choose Topic</title>
    </head>
    <body>
        <h1>Hello <%=curUser.getName()%></h1>
        <table border="1" width='100%'>
            <thead>
                <tr>
                    <th>Topic's name</th>
                    <th>Posted By</th>
                </tr>
            </thead>
            <tbody>
                <sql:query var="topics" dataSource="${ksz}">
                    SELECT topic,pid,id FROM topics
                </sql:query>
                <c:forEach var="topic" items="${topics.getRowsByIndex()}">
                    <tr>
                    <sql:query var="usr" dataSource="${ksz}">
                        SELECT nick_name FROM user where id = ?
                        <sql:param value="${topic[1]}"/>
                    </sql:query>
                <td>
                    <c:url var="rediTop" value="/TopicsDisplay.jsp">
                        <c:param name="id" value="${topic[2]}"/>
                    </c:url>
                    <a href="${rediTop}">${topic[0]}</a>
                </td>
                <td>
                    <c:forEach var="posted" items="${usr.getRowsByIndex()}">
                        ${posted[0]}
                    </c:forEach>
                </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <br>
        <a href="newTopic.jsp">Create new topic</a>
    </body>
</html>
