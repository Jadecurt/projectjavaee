<%@page import="com.model.User"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="curUser" class="com.model.User" scope="request"></jsp:useBean>
<%
    curUser = (User) request.getSession().getAttribute("CurUser");
%>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Profile </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- nav bar  -->
	<link rel="stylesheet" href="css/nav.css">
	
	
	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style1.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<nav class="menu">
	<ul class="active">
		<li class="current-item"><a href="#">Home</a></li>
		<li><a href="../KidsSupportZoneApp1/ChooseTopic.jsp">Topic</a></li>
		<li><a href="#">Log Out</a></li>
	</ul>
	

	<a class="toggle-nav" href="#">&#9776;</a>
	</nav>

	
	<div id="fh5co-about" class="animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Your Profile</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<ul class="info">
                                            <li><span class="first-block">Full Name: <%=curUser.getName()%> </span><span class="second-block"></span></li>
						<li><span class="first-block">Phone: <%=curUser.getAge()%></span><span class="second-block"></span></li>
						<li><span class="first-block">Email: <%=curUser.getEmail()%></span><span class="second-block"></span></li>
						<li><span class="first-block">Nick Name: <%=curUser.getNick_name()%></span><span class="second-block"></span></li>
						<li><span class="first-block">Role: <%=curUser.getRole()%></span><span class="second-block"></span></li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>

	
	

	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- nav -->
	<script src="js/nav.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Easy PieChart -->
	<script src="js/jquery.easypiechart.min.js"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="js/google_map.js"></script>
	
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

