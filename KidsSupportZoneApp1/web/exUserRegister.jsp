<%-- 
    Document   : exUserRegister
    Created on : Nov 23, 2018, 9:12:29 PM
    Author     : Habib
--%>

<%@ page import="com.model.User"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="mySql" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/kidssupportzone?zeroDateTimeBehavior=convertToNull" user="root" password=""/>
        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register User</title>
    </head>
    <body>
        <c:set var="email" value="${param.email}"/>
        <c:set var="name" value="${param.name}"/>
        <c:set var="age" value="${param.nick_name}"/>
        <c:set var="age" value="${param.age}"/>
        <c:set var="gender" value="${param.gender}"/>
        <c:set var="password" value="${param.password}"/>
        <c:set var="role" value="${param.role}"/>
        <c:choose>
            <c:when test="${(name!='')||(age!='')||(gender!='')||(email!='')||(password!='')}">
                <sql:update var="source" dataSource="${mySql}">
                    INSERT INTO user (email,name, nick_name, age,gender,password,role) Values(?,?,?,?,?,?,?)
                    <sql:param value="${email}"/>
                    <sql:param value="${name}"/>
                    <sql:param value="${nick_name}"/>
                    <sql:param value="${age}"/>
                    <sql:param value="${gender}"/>
                    <sql:param value="${password}"/>
                    <sql:param value="${role}"/>
                </sql:update>
                <% response.sendRedirect("index.jsp");%>
            </c:when>
            <c:otherwise>
                <jsp:useBean id="errListBean" class="java.util.LinkedList" scope="request">
                    <c:if test="${(email=='')}">
                        <%
                            errListBean.addLast("Please enter your email.");
                        %>
                    </c:if>
                    <c:if test="${name==''}">
                        <%
                            errListBean.addLast("Please Enter your name");
                        %>
                    </c:if>
                    <c:if test="${nick_name==''}">
                        <%
                            errListBean.addLast("Please Create a nick name");
                        %>
                    </c:if>
                    <c:if test="${(age=='')}">
                        <%
                            errListBean.addLast("Please Enter your age");
                        %>
                    </c:if>
                    <c:if test="${(gender=='')}">
                        <%
                            errListBean.addLast("Please enter your gender.");
                        %>
                    </c:if>
                    <c:if test="${(password=='')}">
                        <%
                            errListBean.addLast("Please enter your password.");
                        %>
                    </c:if>
                </jsp:useBean>
                <jsp:useBean id="AddBean" class="com.model.User" scope="request">
                    <jsp:setProperty name="AddBean" property="email" value="${email}"/>
                    <jsp:setProperty name="AddBean" property="name" value="${name}"/>
                    <jsp:setProperty name="AddBean" property="nick_name" value="${nick_name}"/>
                    <jsp:setProperty name="AddBean" property="age" value="${age}"/>
                    <jsp:setProperty name="AddBean" property="gender" value="${gender}"/>
                    <jsp:setProperty name="AddBean" property="password" value="${password}"/>
                    <jsp:setProperty name="AddBean" property="role" value="${role}"/>
                </jsp:useBean>
                <% 
                    request.setAttribute("userObj",AddBean);
                    request.setAttribute("errList",errListBean);
                    RequestDispatcher view = request.getRequestDispatcher("/userRegister.jsp");
                    view.forward(request, response);
                %>
            </c:otherwise>
        </c:choose>

    </body>
</html>