<%-- 
    Document   : session
    Created on : Dec 10, 2018, 12:13:32 AM
    Author     : Asyraf
--%>

<%@page import="com.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${sessionScope.get('curUser')==null}">
    <%=
        ((User)request.getSession().getAttribute("curUser")).getName()
    %>
</c:if>