<%-- 
    Document   : newTopic
    Created on : Dec 10, 2018, 3:01:33 PM
    Author     : ACER
--%>
<%@ page import="com.model.Topic"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="ksz" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/kidsupportzone" user="root" password=""/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Topic</title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th>Topic Title</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" name="title" id="title"></td>
                    <td><textarea name="desc" id="desc"></textarea></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
